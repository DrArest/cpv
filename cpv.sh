#!/bin/bash
# ---------------------------------------------------------------------------
# cpv.sh - ComparePkgVersions

# Copyright 2014, Orest Pazdriy <arest@home-nas>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License at (http://www.gnu.org/licenses/) for
# more details.

# ---------------------------------------------------------------------------
shopt -s extdebug
PROGNAME=${0##*/}
VERSION="1.0"
declare -a OPTIONS=( "$@" )
declare -A pkg
source ./bashlib_common.sh
source ./progress_bar.sh
#enable_trapping

width=$(tput cols)
height=$(tput lines)
let RES_COL=${width}-20
let RES_LINE=${height}-1
#_VERBOSE=1

clean_up() { # Perform pre-exit housekeeping
	if [[ $COMPLETED && $(has_value CMDFILE) ]]; then 
		echo "echo ====== Program abnormal terminated by user." > $_CMDFILE
	fi
    return
}

error_exit() {
    echo -e "${PROGNAME}: ${1:-"Unknown Error"}" >&2
    clean_up
    exit 1
}

graceful_exit() {
    clean_up
    exit
}

signal_exit() { # Handle trapped signals
    case $1 in
        INT)    error_exit "Program interrupted by user" ;;
        TERM)   echo -e "\n$PROGNAME: Program terminated" >&2 ; graceful_exit ;;
        *)      error_exit "$PROGNAME: Terminating on unknown signal" ;;
    esac
}

usage() {
    echo -e "Usage: $PROGNAME [-h|--help] [ [-v|--verbose] | [-q|--quite] ] [-y|--yes] [-n|--dry] [ -d|--downgrade ] [ -L [FILE]|--load[=FILE] ] [ -c [ FILE ]|--cmd-file[=FILE] ] [-o [ FILE ] |--output[=FILE]] [-p [PKG_MANAGER]|--pkgmgr[=PKG_MANAGER]] [PACKAGE1] [PACKAGE2] ... [PACKAGEn]"
}

help_message() {
	echo "
    $PROGNAME ver. $VERSION

    $(usage)

    Options:
    -h, --help  	Display this help message and exit.
    -v, --verbose   Verbose
	-q, --quite 	Display less messages
	-y, --yes		Always answer YES
	-c FILE, --cmd=FILE		Create file FILE with uninstall commands
	-n, --dry		Don't perform any changes
	-l FILE, --load-dpkg=FILE	Load dpkg packages list from file
	-L FILE, --load-apt=FILE	Load apt packages list from file
	-d, --downgrade	Downgrade packages
	-P, --prepare	Prepare data (download data from 'dpkg -l' and 'apt search' commands
	-o FILE, --output=FILE	Write logs to FILE
	-p PKG_MANAGER, --pkgmgr=PKG_MANAGER	Downgrade packages with different versions, using aptitude by default
	-s, --skip	Skip when installed version is lower than version in apt.

	Arguments:
	PACKAGE1..n - Check only this packages
"
##    EOF
    return
}
name=0
ver=0
print_progress() {
	tput civis
	tput sc
	tput ll
	tput el
	tput cuf $RES_COL
	tput bold
	tput setaf 2
#	tput cup 0 $(($(tput cols)-29))
	printf "%0.0f" "$@"
	printf "%% \n"
	tput rc
	tput sgr0
	tput cnorm
}

status_prepare() {
#	tput tsl
	tput ll
	tput el
#	let RES_COL=$(tput cols)-10
	tput cuf $RES_COL
	tput sc
	tput bold
	echo -en "$1    / $2"
}
status () {
	tput bold
	tput setaf 2
	tput sc;tput cup 0 $(($(tput cols)-29));echo -en "$@";tput rc
	echo -en "$1"
	tput sgr0
}

prepare_data () {
	rm dpkgs-*.txt
	F_DPKG=$(mktemp dpkgs-XXXXX.txt)
	[ $_VERBOSE ] && msg "F_DPKG=$F_DPKG"
	F_APT=$(mktemp apts-XXXXX.txt)
	[ $_VERBOSE ] && msg "F_APT=$F_APT"
	progress=0; progress_total=$(dpkg -l |tail -n +6 |wc -l)
	while read line; do
		local name=$(echo $line |awk '{print $2}')
		local ver=$(echo $line |awk '{print $3}')
		echo "${name%:*} ${ver}" >> $F_DPKG
		[ $_VERBOSE ] && msg "$name $ver - Ok"
		[ ! $_VERBOSE ] && ProgressBar "${progress}" "$progress_total" && (( progress++ ))
	done < <(dpkg -l |tail -n +6)
	echo "Total lines written:"
	echo "	${F_DPKG}:  $(cat $F_DPKG | wc -l)"
	_LINES=$(cat $F_DPKG | wc -l)
	[ $_VERBOSE ] && msg "_LINES=$_LINES"
	PERCENTPERPKG=$(printf "scale=2\n100/%d\n" "$_LINES" |bc )
	[ $_VERBOSE ] && msg "PERCENTPERPKG=$PERCENTPERPKG"
	CURPERCENT=0	
	RES_COL=1
	while read name ver; do
		[ $_VERBOSE ] && echo -en "Progress ${CURPERCENT}%"
		CURPERCENT=$(printf "scale=2\n%.2f + %.2f\n" "$CURPERCENT" "$PERCENTPERPKG" | bc )
		ProgressBar $(printf "%.2f" "${CURPERCENT}") "100"
		local aptline=$(apt search $name  2>/dev/null |grep ^${name}/)
		local aptname=$(echo $aptline | sed 's/\/.*$//')
		local aptver=$(echo $aptline | awk '{print $2}')
		echo "$aptname $aptver" >> $F_APT
	done < <(cat $F_DPKG)
	echo "	${F_APT}:  $(cat $F_APT | wc -l)"
}
	
	
check() {
	local name="${1%:*}"
	local ver="$2"
	[ $_VERBOSE ]  && msg "Checking package $name ver. $ver."	
	
	has_value $_LOADAPT
	if [ $? -eq 0 ]; then
		aptname=$(cat $_LOADAPT |grep "^${name} " | awk '{print $1}')
		aptver=$(cat $_LOADAPT |grep "^${name} " | awk '{print $2}')
	else
		local aptline=$(apt search ${name}  2>/dev/null |grep ^${name}/)
		[ $_VERBOSE ] && msg "apt: $aptline "
		local aptname=$(echo $aptline | sed 's/\/.*$//')
		local aptver=$(echo $aptline | awk '{print $2}')
	fi
	[ $_VERBOSE ] && echo -n "aptname: ==> $aptname"
	[ $_VERBOSE ] && echo -n "ver: ==> $aptver"
	[ $_VERBOSE ] && msg "Comparing installed package $name ver. $ver with available to current release: $aptname ver. ${aptver}."
	unset _SKIPALL
	[ "$ver" \< "$aptver" ] && [ $_SKIP ] && _SKIPALL=1
	if [ "${name}" != "${aptname}" ] ; then
		echo "$name <=> $aptname"
		if [ ! $_QUITE ]; then
			echo 
			echo -e "# $iDGRAY Name mismatch: $iBLUEBG $name $iNORM 		$iORANGEBOLD $name $iDCYANBOLD != $iVIOLETBOLD $aptname $iNORM"
#			echo -n "#"
		fi
	elif [ "$ver" != "$aptver" ]; then
		if [ ! $_QUITE ]; then
			echo 
			if [ "$ver" \< "$aptver" ]; then
		       	if [ ! $_SKIP ]; then
					echo -e "# Version mismatch: $iBLUEBG $name  		$iORANGEBOLD $ver $iDCYANBOLD != $iVIOLETBOLD $aptver $iNORM"
				fi
			else			
				echo -e "# $iDORANGE Version mismatch: $iMAGENTABG $name  		$iORANGEBOLD $ver $iDCYANBOLD != $iVIOLETBOLD $aptver $iNORM"
			fi
		fi
		if [ ! $_SKIPALL ]; then
			cmd="${aptname}=${aptver}" 	
			has_value _CMDFILE
			if [ $? -eq 0 ]; then
				[ $VERBOSE ] && msg "Put command to $_CMDFILE:"
				[ $VERBOSE] && msg "$cmd"	
				[ $VERBOSE ] && msg "_CMDFILE=$_CMDFILE"
				echo "$cmd" >> $_CMDFILE
			else
				fail 102 "_CMDFILE is unset" $BASH_LINENO
			fi				
			[ ! $_QUITE ] && echo -e "$cmd" && log "$cmd"
			if [ $_DOWNGRADE ]; then
				if [ ! $_YES ]; then
					echo -en "# Downgrade package ${iYELLOW}${name}${iNORM} from version ${iYELLOW}${ver}${iNORM} to version ${iGREEN}${aptver}${iNORM}?  "
					read -N 1 -r -t 120 
					echo
					[ ${REPLY^y} != "Y" ] && continue
				fi 
				echo
				[ ! $_DRY ] && duration "$cmd"
				eval "$1='$?'"
				return $?
			fi
	#		echo -n "#"
		fi
	fi
}

downgrade () {
	local name="$1"
	local ver="$2"
	local total="$3"
	local idx="$4"
	PERCENTPERPKG=$(echo $(printf "100/%d\n" "$total") |bc )
	PERCENTPERPKG=$(printf "%.2f" "$PERCENTPERPKG")
	[ $_VERBOSE ] && msg "PERCENTPERPKG=$PERCENTPERPKG"
	CURPERCENT=0	
	CURPERCENT2=0	
	[ $_VERBOSE ] && _INFO "dpkg: $name"
	[ $_VERBOSE ] && _INFO "${ver}"
	[ $_VERBOSE ] && printf "Progress ${iBLUE}%.2f${iNORM}\n" "$CURPERCENT2"
#	CURPERCENT=$(echo "$CURPERCENT" "$PERCENTPERPKG" | awk '{print $1 + $2}')
	CURPERCENT2=$(printf "scale=2\n%d*100/%d\n" "$idx" "$total" | bc)
	CURPERCENT2=$(printf "%.2f" "$CURPERCENT")
#	ProgressBar "${CURPERCENT}" "100"
#	ProgressBar "$i" "$total" 
#	draw_progress_bar $CURPERCENT2
	check "$name" "${ver}"
	error=$?
	[ $_VERBOSE ] && msg "Error=$error"
	if [ $error -ne 0 ]; then
		_FAIL "Checking failed with error $?"
	else
		[ $_VERBOSE ] && _OK "Checking completed."
	fi
	COMPLETED=1
}

# Trap signals
trap "signal_exit TERM" TERM HUP
trap "signal_exit INT"  INT

[ ! $_QUITE ] && echo "$PROGNAME v.$VERSION. Copyright (c) Lviv, 2020"  

# set an initial value for the flag
unset _DRY _QUITE _YES _PKGMGR _CMDFILE _DOWNGRADE _SKIP
# read the options
#OPTIONS=( "${OPTIONS[@]}" "--" )
set -- ${OPTIONS[@]}
__TEMP=`getopt -o Ac::d::l::L::no::Psqvyh --long apt,cmd-file::,downgrade::,dry,load-dpkg::,load-apt::,output::,prepare,skip,quite,verbose,yes,help -- "$@"`
eval set -- "$__TEMP"
#echo "OPTIONS=${OPTIONS[@]} _VERBOSE=$_VERBOSE"

# extract options and their arguments into variables.

while true ; do
    case "$1" in
        -v|--verbose) _VERBOSE=1; [ $_VERBOSE ] && msg "Option -v enabled. Argument - not present"; shift ;;
        -q|--quite) [ $_VERBOSE ] && msg "Option -q enabled. Argument - not present"; _QUITE=1 ; shift ;;
        -A|--apt)
			[ $_VERBOSE ] && msg "Option -A enabled."
			_PKGMGR='apt' ; shift ;;
        -n|--dry) [ $_VERBOSE ] && msg "Optiona -n enabled. Argument - not present"; _DRY=1 ; shift ;;
        -o|--output)
			_ARG=$2
			[ $_VERBOSE ] && msg "Option -o enabled."
			LOG_ENABLED=TRUE
            case "$_ARG" in
                "") 
					[ $_VERBOSE ] && msg "OptionArgument not present. Using ${PROGNAME}.log";
					LOG_FILE="${PROGNAME}.log" ; shift 2 ;;
                *) 
					[ $_VERBOSE ] && msg "OptionArgument - $_ARG";
					LOG_FILE=$_ARG ; shift 2 ;;
            esac ;;
        -l|--load-dpkg)
			_ARG=$2
			[ $_VERBOSE ] && msg "Option -l enabled."
            case "$_ARG" in
                "") 
					[ $_VERBOSE ] && msg "Option Argument not present. Using ./dpkgs.txt"
					_LOADDPKG='./dpkgs.txt' ; shift 2 ;;
                *) 
					[ $_VERBOSE ] && msg "Option Argument $_ARG"
					_LOADDPKG=$_ARG ; shift 2 ;;
            esac ;;
        -L|--load-apt)
			_ARG=$2
			[ $_VERBOSE ] && msg "Option -L enabled."
            case "$_ARG" in
                "") 
					[ $_VERBOSE ] && msg "OptionArgument not present. Using ./apts.txt"
					_LOADAPT='./apts.txt' ; shift 2 ;;
                *) 
					[ $_VERBOSE ] && msg "OptionArgument $_ARG"
					_LOADAPT=$_ARG ; shift 2 ;;
            esac ;;
		-P|--prepare) [ $_VERBOSE ] && msg "Option -P enabled. Argument - not present"; prepare_data; graceful_exit; shift ;;
		-s|--skip) [ $_VERBOSE ] && msg "Option -s enabled. Argument - not present";_SKIP=1; shift ;;
		-d|--downgrade) [ $_VERBOSE ] && msg "Option -d enabled. Argument - not present"; _DOWNGRADE=1; shift ;;
		-y|--yes) [ $_VERBOSE ] && msg "Option -y enabled. Argument - not present"; _YES=1 ; shift ;;
		-h|--help) [ $_VERBOSE ] && msg "Option -h enabled. Argument - not present"; help_message; graceful_exit ;;
		-c|--cmd-file)	
			_ARG=$2
			[ $_VERBOSE ] && msg "Option -c enabled."
	        case "$_ARG" in
    	    	"") 
					[ $_VERBOSE ] && msg "OptionArgument not present. Using cmd-${PROGNAME}";
					_CMDFILE="cmd-${PROGNAME}" ; shift 2 ;;
				-*)	
					[ $_VERBOSE ] && msg "OptionArgument not present. Using cmd-${PROGNAME}";
					_CMDFILE="cmd-${PROGNAME}" 
					shift ;;
        		*) 
					[ $_VERBOSE ] && msg "OptionArgument - $_ARG";
					_CMDFILE=$_ARG ; shift 2;;
           	esac 
			[ $_VERBOSE ] && msg "_CMDFILE=$_CMDFILE"
			if [ -f ./$_CMDFILE ]; then
				while true; do
					echo -en "Overwrite file $_CMDFILE? (O)Owerwrite/(A)Append/(Q)Quit :"
					read -N1 -r -t 120;
					case "$REPLY" in 
						O|o) 	echo > $_CMDFILE ; break ;;
						A|a)	break ;;
						Q|q)	fail 101 "Intrerrupted by user."  ;;
						*)		echo "Unknown command.";;
					esac
				done
			fi		
		;;
		--) shift ; break ;;
		*) echo "Unknown option!" ; usage; error_exit ;;
    esac
done

#	set -- ${OPTIONS[@]}
	[ $_VERBOSE ] && msg "\$@=$@"
	i=0
#	setup_scroll_area
	if [ $# -gt 0 ]; then
		[ $_VERBOSE ] && msg "Arguments present: $@"
		if [ $(has_value "${_LOADDPKG}") ] || [ $(has_value "${_LOADAPT}") ]; then
			_CRITICAL "Command line arguiments provided, so packages could not be loaded from files."
			die "Packages list could not be loaded due to command line arguments provided"
		fi
		for (( i=1; i<$#+1; i++)) do
				[ $_VERBOSE ] && msg "Parameter \$1=$1, i=${i}, \${#@}=${#@}"
				read name ver < <(echo $(dpkg -l |awk '{print $2, $3}' |grep "^${1}\s"))
				[ $_VERBOSE ] && msg "Read variables name=$name ver=$ver";
				if [[ -z "$name" || -z "$ver" ]]; then
					die "Unknown package"
					continue
				fi
				#ver=$(dpkg -l | awk '{print $2 $3}' |grep openssl)
				pkg[$name]=$ver
				(( j = i - 1 ))
				#spin
				downgrade "$name" "$ver" "${#@}" "$j"
		done
	#	endspin
	else
		has_value "$_LOADDPKG"
		if [ $? -eq 0 ]; then
			_LINES=$(cat $_LOADDPKG |wc -l)
			[ $_VERBOSE ] && msg "_LINES=$_LINES"
			while read name ver; do
				pkg[$name]=$ver
				downgrade "$name" "$ver" "$_LINES" "$i"
				(( i++ ))
				spin
			done
			endspin
		else
			_LINES=$(dpkg -l | tail -n +6 | awk '{print $2,$3}' | wc -l)
			[ $_VERBOSE ] && msg "_LINES=$_LINES"
			dpkg -l |tail -n +6 | while read line; do
				name=$(echo $line |awk '{print $2}')
				ver=$(echo $line |awk '{print $3}')
				pkg[$name]=$ver
				downgrade "$name" "$ver" "$_LINES" "$i"
				(( i++ ))
				spin
			done
			endspin
		fi
	fi
