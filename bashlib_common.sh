#!/bin/bash
# ##############################
# Bash Library (Common Function)
# ##############################
#
# Pre-Author(BSFL): Louwrentius <louwrentius@gmail.com>
# Author: Orest Pazdriy
#
# Copyright © 2013
#
# Released under the curren GPL version.
#
# Description:
#
# This is a shell script library. It contains functions that can be called by
# programs that include (source) this library. 
#
# By simply sourcing this library, you can use all available functions as 
# documented on the projects page.
#
#
function init {
	
    : ${__ScriptVersion:=2.1}

    [ -f ${BASH_SOURCE[0]}.conf ] && source ${BASH_SOURCE[0]}.conf
	BASHLIB_NAME=${BASH_SOURCE[0]##*/}
	BASHLIB_PATH=${BASH_SOURCE[0]%/*}
	PROG_NAME=${BASH_SOURCE[2]##*/}
	PROG_PATH=${BASH_SOURCE[2]%/*}
#	echo $BASHLIB_NAME $BASHLIB_PATH $PROG_NAME $PROG_PATH
    : ${DEBUG:=}
    : ${LOG_ENABLED:=}
    : ${SYSLOG_ENABLED:=}
    : ${SYSLOG_TAG:=$PROG_NAME}
    : ${LOGDATEFORMAT:=%b %e %H:%M:%S}
    : ${LOG_FILE:=${PROG_NAME}.log}
    : ${USAGE:=}
    : ${OPT_SHORT:=}
    : ${OPT_LONG:=}
    : ${__VERBOSE:=}
    : ${__FUNC:=}
    : ${__VAR:=}


    # Use colours in output.
    RED="tput setaf 1"
    GREEN="tput setaf 2"
    YELLOW="tput setaf 3"
    BLUE="tput setaf 4"
    MAGENTA="tput setaf 5"
    CYAN="tput setaf 6"
    LIGHT_BLUE="$CYAN"
    BOLD="tput bold"
    DEFAULT="tput sgr0"
#    DEFAULT="echo \e[0m"
    COL8="tput setaf 8"
    COL9="tput setaf 9"
    
#    TCOL8="\e[${fgbg};5;${color}m ${color}\t\e[0m"


    RED_BG="tput setab 1"
    GREEN_BG="tput setab 2"
    YELLOW_BG="tput setab 3"
    BLUE_BG="tput setab 4"
    MAGENTA_BG="tput setab 5"
    CYAN_BG="tput setab 6"
    COL8_BG="tput setab 8"
    COL9_BG="tput setab 9"
    
	TCOL8="echo -en \033[5;30;41m"
	TCOL9="echo -en \033[1;33;40m"
	TNORM="echo -en \033[0m"

#Normal
	iNORM="\e[0m"
	iDEFAULT=$iNORM
	iRED="\e[0;49;31m"
	iGREEN="\e[0;49;32m"
	iYELLOW="\e[0;49;33m"
	iLBLUE="\e[0;49;34m"
	iMAGENTA="\e[0;49;35m"
	iCYAN="\e[0;49;36m"
	iWHITE="\e[0;49;37m"
	iDCYAN="\e[0;49;90m"
	iDORANGE="\e[0;49;91m"
	iDGRAY="\e[0;49;92m"
	iGRAY="\e[0;49;93m"
	iLGRAY="\e[0;49;94m"
	iVIOLET="\e[0;49;95m"

	iREDBG="\e[7;49;31m"
	iGREENBG="\e[7;49;32m"
	iYELLOWBG="\e[7;49;33m"
	iLBLUEBG="\e[7;49;34m"
	iMAGENTABG="\e[7;49;35m"
	iCYANBG="\e[7;49;36m"
	iWHITEBG="\e[7;49;37m"
	iDCYANBG="\e[7;49;90m"
	iDORANGEBG="\e[7;49;91m"
	iDGRAYBG="\e[7;49;92m"
	iGRAYBG="\e[7;49;93m"
	iLGRAYBG="\e[7;49;94m"
	iVIOLETBG="\e[7;49;95m"
# BOLD
	iORANGEBOLD="\e[1;49;31m"
	iDGRAYBOLD="\e[;49;3332m"
	iGRAYBOLD="\e[1;49;33m"
	iLGRAYBOLD="\e[1;49;34m"
	iVIOLETBOLD="\e[1;49;35m"
	iWHITEBOLD="\e[1;49;37m"
	iDCYANBOLD="\e[1;49;90m"

    # Bug fix for Bash, parsing exclamation mark.
    set +o histexpand
}

function has_value {
[ ${USAGE} ] && ( $BOLD; echo -en "${TCOL9}${FUNCNAME}$TNORM";$DEFAULT; echo " VAR" ; echo -e "	check if VAR has value and returns 0 or 1" ) && return 0
    [[ ${1} ]] && [[ -n ${!1} ]] && return 0 || return 1
}

function directory_exists {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " DIR" ; echo -e "	check if directory DIR exists and returns 0 or 1" ) && return 0
    [[ -d "$1" ]] && return 0 || return 1
}

function file_exists {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " FILENAME" ; echo -e "	check if file FILENAME exists and returns 0 or 1" ) && return 0
    [[ -f "$1" ]] && return 0 || return 1
}

function tolower {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " STR" ; echo -e "	convert string STR to lower case" ) && return 0
    echo "$1" | tr '[:upper:]' '[:lower:]'
}

function toupper {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " STR" ; echo -e "	convert string STR to upper case" ) && return 0
    echo "$1" | tr '[:lower:]' '[:upper:]'
}

function trim {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " STR" ; echo -e "	trim string STR to first part separated by white space" ) && return 0
    echo $1
}

function greater {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " STR1 STR2" ; echo -e "	compare string STR1 with string STR2. Return 0 if STR1 is greater than STR2 alphabetically." ) && return 0
	local STR1=$1
	local STR2=$2
	if [ "$STR1" > "$STR2" ]; then
		return 0
	else
		return 1
	fi
}

function show_usage () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " STR" ; echo -e "	print STR and exit with exit code 1" ) && return 0
    echo "$1"
    exit 1
}

function log2syslog () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	if \$SYSLOG_ENABLED is set, write to syslog message MSG marked by \$SYSLOG_TAG and prepended with a date & time in syslog format" ) && return 0
    [[ $SYSLOG_ENABLED ]] && logger -t "$SYSLOG_TAG" " ${1}"
}

function log () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	put MSG into a log file \$LOG_FILE if \$LOG_ENABLED is set and/or write to syslog if \$SYSLOG_ENABLED is set, prepend with a date & time in \$LOGDATEFORMAT format" ) && return 0

    if [[ $LOG_ENABLED || $SYSLOG_ENABLED ]]; then  
        local LOG_MESSAGE="$1" 
        DATE=`date +"$LOGDATEFORMAT"`
        has_value LOG_MESSAGE && LOG_STRING="$DATE $LOG_MESSAGE" || LOG_STRING="$DATE -- empty log message, no input received --"

        [[ $LOG_ENABLED ]] && echo "$LOG_STRING" >> "$LOG_FILE"

        [[ $SYSLOG_ENABLED ]] && log2syslog "$LOG_MESSAGE"
    fi
}


function msg () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG [COLOR]" ; echo -e "	echo command replacement, displays message MSG using color \$2 (optional), also log message \$1" ) && return 0
    MESSAGE="$1"
    COLOR="$2"
    has_value COLOR && COLOR="$DEFAULT"
    if has_value "MESSAGE"; then
        $COLOR; echo -e "$MESSAGE"; $DEFAULT; log "$MESSAGE"
    else
        echo "-- no message received --"; log "$MESSAGE"
    fi
}

function msg_status () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG STATUS" ; echo -e "	echo message MSG, displays status STATUS at the end of the line" ) && return 0
    msg "${1}"
    display_status "${2}"
}

function _EMERGENCY () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _ALERT () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _CRITICAL () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _ERROR () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _WARNING () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _NOTICE () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _INFO () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _DEBUG () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _OK () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _NOT_OK () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _FAIL () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _SUCCESS () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function _PASSED () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display status ${FUNCNAME/_/} at the end of the line MSG" ) && return 0
    msg_status "${1}" "${FUNCNAME/_/}"
}

function display_status () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " STATUS " ; echo -e "	display status STATUS (EMERGENCY | ALERT | CRITICAL | ERROR | WARNING | NOTICE | INFO | DEBUG | OK | NOT_OK | PASSED | SUCCESS | FAILED)" ) && return 0
    local STATUS="$1"
    case $STATUS in 
    EMERGENCY )
            STATUS="${iRED}EMERGENCY"  ;;
    ALERT )
            STATUS="${iRED}  ALERT  " ;;
    CRITICAL )
            STATUS="${iRED}CRITICAL " ;;
    ERROR )
            STATUS="${iRED}  ERROR  " ;;
    WARNING )
            STATUS="${iYELLOW} WARNING " ;;
    NOTICE )
            STATUS="${iBLUE} NOTICE  " ;;
    INFO )
            STATUS="${iLBLUE}  INFO   " ;;
    DEBUG )
            STATUS="${iNORM}  DEBUG  " ;;    
    OK  ) 
            STATUS="${iGREEN}   OK    " ;;
    NOT_OK)
            STATUS="${iRED} NOT OK  " ;;
    PASSED ) 
            STATUS="${iGREEN} PASSED  " ;;
    SUCCESS ) 
            STATUS="${iGREEN} SUCCESS " ;;
    FAILURE | FAILED )
            STATUS="${iRED} FAILED  " ;;
    *)
            STATUS="${iYELLOW}UNDEFINED" ;;
    esac

    position_cursor () {
        let RES_COL=`tput cols`-12
        tput cuf $RES_COL
        tput cuu1
    }

    position_cursor
    echo -en "["
#    $DEFAULT
#    $BOLD
#    $COLOR
    echo -en "${STATUS}${NORM}"
    $DEFAULT
    echo "]"
    log "Status = ${STATUS}${iNORM}"
}

function fail () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " MSG" ; echo -e "	display message MSG and exit with error status" ) && return 0
    ERROR="$?"
    MSG="$1"
    [[ $ERROR -ne "0" ]] && fail "$MSG" && exit "$ERROR"
}

function cmd () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " COMMAND" ; echo -e "	run command COMMAND, display if the command succeeded or not" ) && return 0
    COMMAND="$1"
    msg "Executing: $COMMAND"
    RESULT=`$COMMAND 2>&1`
    ERROR="$?"
    MSG="Command: ${COMMAND:0:29}..."
    tput cuu1
    if [ "$ERROR" -eq 0 ]; then
        msg_ok "$MSG"
        [[ "$DEBUG" -eq 1 ]] && msg "$RESULT"
    else
        fail "$MSG"
        log "$RESULT"
    fi

    return "$ERROR"
}

function now () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo "" ; echo -e "	print seconds since 1970-01-01 00:00:00 UTC" ) && return 0
   echo $(date +%s)
}

function elapsed () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " START_TIME STOP_TIME" ; echo -e "	display how long command take to execute, where START_TIME,STOP_TIME - start/stop time in seconds since 1970-01-01 00:00:00 UTC" ) && return 0
    
    START="$1"
    STOP="$2"
    echo $(( STOP - START ))
}

function duration () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " CMD" ; echo -e "	display duration of command CMD in seconds since 1970-01-01 00:00:00 UTC" ) && return 0
    CMD=$1
    START=$(date +%s)
	cmd "$CMD"
    STOP=$(date +%s)
	(( DURATION = $STOP - $START ))
	eval "$1='$DURATION'"
}

function die {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " EXITCODE MSG [CALLER]" ; echo -e "	Prints an error message MSG to stderr, exits with the return code EXITCODE, the message is also logged, where CALLER - number of line" ) && return 0
    local -r err_code="$1"
    local -r err_msg="$2"
    local -r err_caller="${3:-$(caller 0)}"

    fail "ERROR: $err_msg"
    fail "ERROR: At line $err_caller"
    fail "ERROR: Error code = $err_code"
    exit "$err_code"
} >&2

function die_if_false {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " EXITCODE MSG [CALLER]" ; echo -e "	Check if a return code EXITCODE indicates an error and prints an error message MSG to stderr and exits with the return code EXITCODE. The error is also logged. CALLER - number of line." ) && return 0
    local -r err_code=$1
    local -r err_msg=$2
    local -r err_caller=$(caller 0)

    if [[ "$err_code" != "0" ]]
    then
        die $err_code "$err_msg" "$err_caller"
    fi
} >&2

function die_if_true {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " EXITCODE MSG" ; echo -e "	Dies when error code EXITCODE is true, where MSG - error message." ) && return 0
    local -r err_code=$1
    local -r err_msg=$2
    local -r err_caller=$(caller 0)

    [[ "$err_code" == "0" ]] && die $err_code "$err_msg" "$err_caller"
} >&2

function str_replace () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " ORIG TEXT CHANGE_TO_STR"; echo -e "	Search string ORIG inside a string TEXT and replace it with CHANGE_TO_STR." ) && return 0
    local ORIG="$1"
    local DEST="$2"
    local DATA="$3"
    echo "$DATA" | sed "s/$ORIG/$DEST/g"
}

function str_replace_in_file () {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " ORIG CHANGE_TO_STR " ; echo -e "	Search string ORIG in file FILE and replace it with CHANGE_TO_STR using ed editor." ) && return 0
    local ORIG="$1"
    local FILE="$2"
    local DEST="$3"

    has_value FILE 
    die_if_false $? "Empty argument 'file'"
    file_exists "$FILE"
    die_if_false $? "File does not exist"

    printf ",s/$ORIG/$DEST/g\nw\nQ" | ed -s "$FILE" > /dev/null 2>&1
    return "$?"
}

function ProgressBar {
[ ${USAGE} ] && (  $BOLD; echo -en "${TCOL9}${FUNCNAME}${TNORM}"; echo " PROGRESS TOTAL " ; echo -e "	Display progress using PROGRESS value." ) && return 0
# Process data
    let _progress=(${1}*100/${2})/100
    let _done=(${_progress}*4)/10
    let _left=40-$_done
# Build progressbar string lengths
    _fill=$(printf "%${_done}s")
    _empty=$(printf "%${_left}s")

# 1.2 Build progressbar strings and print the ProgressBar line
# 1.2.1 Output example:                           
# 1.2.1.1 Progress : [########################################] 100%
printf "\rProgress : [${_fill// /\#}${_empty// /-}] ${_progress}%%"

}

sp="/-\|"
sc=0
spin() {
   printf "\b${sp:sc++:1}"
   ((sc==${#sp})) && sc=0
}
endspin() {
   printf "\r%s\n" "$@"
}


function show-func () {
	local func_list=$(awk '/^[ \t]*function/{print $2}' $BASH_SOURCE |while read line; do echo -en " $line"; done)
	if [ "$__VERBOSE" ]; then
#        $BOLD
        _DEBUG "Functions: $func_list"
  		echo "$BASHLIB_NAME functions:"
#		$DEFAULT
		for func_item in ${func_list}; do 
			[ ${func_item} == "functions_usage" ] && continue
			${func_item}
		done
	else
		echo -en "$BASHLIB_NAME functions:\n ${func_list}\n"
	fi
}

		
function show-var () {
		local var_list=$(awk '/^[ \t]*: /{sub(/^[ \t]*: \$\{/,"");gsub(/:.*$/,"");print}' $BASH_SOURCE |while read line; do echo -en " $line"; done)  
		echo "$BASHLIB_NAME variables:"
        echo $var_list
}

#===  FUNCTION  ================================================================
#         NAME:  usage
#  DESCRIPTION:  Display usage information.
#===============================================================================
function usage ()
   {
#   		cat <<- EOT
	echo -en "   
Usage : ${iBLUE} $0${iNORM} [${iDGRAY}options${iNORM}]
   
Options:
  	-h|help		   Display this message
	-d|debug	   Use debug mode
	-f|func  	   Show used functions
	-w|var   	   Show used variables
	-v|verbose	   Increase verbosity
   	-V|version   	   Display script version
"  
#EOT
}    # ----------  end of function usage  ----------
   
   #-----------------------------------------------------------------------
   #  Handle command line arguments
   #-----------------------------------------------------------------------
	init
	if [ "$BASHLIB_NAME" == "$PROGNAME" ]; then
  		 __BASHLIB_TEMP=$(getopt -o hdfwvV --long help,debug,func,var,verbose,version -- "$@" )
		eval set -- "$__BASHLIB_TEMP"
		if [ $# -gt 0 ]; then
			while true; do
				case "$1" in 
	   				-h|--help     )  usage; shift; return 0   ;;
				   	-d|--debug  )  __DEBUG=1 ; shift  ;;
				  	-f|--func  )  __FUNC=1 ; shift  ;;
				   	-w|--var  ) 	__VAR=1 ; shift  ;;
				  	-v|--verbose  )  __VERBOSE=1 ; shift  ;;
				  	-V|--version  )  echo "$0 -- Version $__ScriptVersion"; return 0   ;;
	               	*)  echo -e "\n  Option does not exist : $OPTARG\n"; usage; shift; return 1   ;;
	    	 	esac    # --- end of case ---
			done
	   		[ "${__FUNC}" ] && USAGE=1 && show-func
			[ "${__VAR}" ] && USAGE=1 && show-var
		fi
		echo "$BASHLIB_NAME -- Version $__ScriptVersion. Copyright (c) Lviv, 2020. All rights reserved." 
		exit 0
	else
		return 0
  	fi
# vim: ts=4 sw=4 sts=4
